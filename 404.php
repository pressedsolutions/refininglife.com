<?php

get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<!-- start content container -->
<div class="container dmbs-content">

    <div class="col-md-12 dmbs-main">

        <h2 class="page-header">Oops! We can&rsquo;t find that page&hellip;</h2>
        <p>Looks like there&rsquo;s nothing at that location. Maybe try a search?</p>
        <?php get_search_form(); ?>

    </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>
