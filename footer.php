<?php
if ( is_singular( array( 'post' ) ) && get_field( 'show_cta' ) ) {
    echo get_small_cta( 'bottom' );
}
?>
    <div class="dmbs-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 dmbs-left footertop">
                    <?php dynamic_sidebar( 'footertop' ); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 dmbs-left">
                    <?php dynamic_sidebar( 'footer1' ); ?>
                </div>
                <div class="col-md-3 dmbs-left">
                    <?php dynamic_sidebar( 'footer2' ); ?>
                </div>
                <div class="col-md-3 dmbs-left">
                    <?php dynamic_sidebar( 'footer3' ); ?>
                </div>
                <div class="col-md-3 dmbs-left">
                    <?php dynamic_sidebar( 'footer4' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 dmbs-left footerfull">
                    <?php dynamic_sidebar( 'footerfull' ); ?>
                </div>
            </div>
        </div>
    </div><!-- .dmbs-footer -->

<?php wp_footer(); ?>
</body>
</html>
