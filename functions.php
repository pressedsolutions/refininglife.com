<?php

/**
 * Enqueue custom stylesheets and theme script
 */
function lp_custom_styles() {
    wp_dequeue_style( 'bootstrap.css' );
    wp_dequeue_style( 'stylesheet' );

    if ( $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ) {
        wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/css/style.css' );
    } else {
        wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/css/style.min.css' );
    }

    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,700italic|Roboto:300,300italic,400,400italic|Source+Sans+Pro:400,400italic,600,600italic' );

    wp_enqueue_script( 'custom-theme', get_stylesheet_directory_uri() . '/js/theme.min.js', array( 'jquery' ), filemtime( get_stylesheet_directory() . '/js/theme.min.js' ), true );
}
add_action( 'wp_enqueue_scripts', 'lp_custom_styles', 15 );

/**
 * Add tracking scripts in footer
 */
function lp_tracking_scripts() { ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script src="//load.sumome.com/" data-sumo-site-id="559b0e76695f92fc5993bdd32804d640bde36f6e732a8cfa3dddf16670c96a28" async="async"></script>
    <script type="text/javascript">
      var trackcmp_email = '';
      var trackcmp = document.createElement("script");
      trackcmp.async = true;
      trackcmp.type = 'text/javascript';
      trackcmp.src = '//trackcmp.net/visit?actid=475141807&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
      var trackcmp_s = document.getElementsByTagName("script");
      if (trackcmp_s.length) {
        trackcmp_s[0].parentNode.appendChild(trackcmp);
      } else {
        var trackcmp_h = document.getElementsByTagName("head");
        trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
      }
    </script>

    <script type="text/javascript">
        adroll_adv_id = "VZ7TQ3BM6ZDS7EJM3XVHBH";
        adroll_pix_id = "FZJ5EZSXHVBTPBVKGTBN4R";
        (function () {
            var _onload = function(){
                if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
                if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
                var scr = document.createElement("script");
                var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                scr.setAttribute('async', 'true');
                scr.type = "text/javascript";
                scr.src = host + "/j/roundtrip.js";
                ((document.getElementsByTagName('head') || [null])[0] ||
                    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            };
            if (window.addEventListener) {window.addEventListener('load', _onload, false);}
            else {window.attachEvent('onload', _onload)}
        }());
    </script>
<?php
}
add_action( 'wp_footer', 'lp_tracking_scripts' );

/**
 * Modify the menu appearance
 * @param string $output HTML string
 */
function custom_menu_display( $output ) {
    echo preg_replace( '| class="dropdown-toggle" data-toggle="dropdown" data-target="#"(.+?)<b |', ' class="a-stripped" $1</a><a href="#" class="dropdown-toggle a-caret" data-toggle="dropdown" data-target="#"><b ', $output, -1);
}
add_filter( 'tc_menu_display', 'custom_menu_display' );

/**
 * Add HTML5 support for search form
 */
add_theme_support( 'html5', array( 'search-form' ) );

/**
 * Add custom sidebars
 */
function lp_custom_sidebars() {
    register_sidebar(
        array(
        'name' => 'Footer Top Full',
        'id' => 'footertop',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
    register_sidebar(
        array(
        'name' => 'Footer 1',
        'id' => 'footer1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
    register_sidebar(
        array(
        'name' => 'Footer 2',
        'id' => 'footer2',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
    register_sidebar(
        array(
        'name' => 'Footer 3',
        'id' => 'footer3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
    register_sidebar(
        array(
        'name' => 'Footer 4',
        'id' => 'footer4',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
    register_sidebar(
        array(
        'name' => 'Footer Bottom Full',
        'id' => 'footerfull',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
    register_sidebar(
        array(
        'name' => 'Search Page Bottom',
        'id' => 'searchfull',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
}
add_action( 'widgets_init', 'lp_custom_sidebars' );

/**
 * Set ACF local JSON save directory
 * @param  string $path ACF local JSON save directory
 * @return string ACF local JSON save directory
 */
function lp_acf_json_save_point( $path ) {
    return get_stylesheet_directory() . '/acf-json';
}
add_filter( 'acf/settings/save_json', 'lp_acf_json_save_point' );

/**
 * Set ACF local JSON open directory
 * @param  array $path ACF local JSON open directory
 * @return array ACF local JSON open directory
 */
function lp_acf_json_load_point( $path ) {
    $paths[] = get_stylesheet_directory() . '/acf-json';
    return $paths;
}
add_filter( 'acf/settings/load_json', 'lp_acf_json_load_point' );

/**
 * Add theme options pages
 */
if ( function_exists('acf_add_options_page') ) {
    acf_add_options_page( array(
        'page_title'    => 'Refining Life Options',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false,
    ) );
}

/**
 * Set thumbnail size
 */
function lp_thumbnail_size() {
    update_option( 'thumbnail_size_w', 500 );
    update_option( 'thumbnail_size_h', 281 );
    update_option( 'thumbnail_crop', 1 );
    add_image_size( 'thumbnail_m', 750, 422, true );
    add_image_size( 'thumbnail_l', 1000, 563, true );
    add_image_size( 'thumbnail_xl', 2000, 1125, true );
    add_image_size( 'header_thumb', 600, 90, true );
}
add_action( 'after_setup_theme', 'lp_thumbnail_size' );

/**
 * Set the_archive_title to the archives page title
 * @param  string $title default archives title
 * @return string name of the archives page
 */
function lp_archives_title( $title ) {
    $blog_page_id = get_option( 'page_for_posts' );
    return get_the_title( $blog_page_id );
}
add_filter( 'get_the_archive_title', 'lp_archives_title' );

/**
 * Get small CTA
 * @param  string $location top or bottom
 * @return string HTML content
 */
function get_small_cta( $location ) {
    if ( get_field( 'small_cta_content', 'option' ) ) {
        $background_image = get_field( 'small_cta_background_image', 'option' );
        $background_color = get_field( 'small_cta_background_color', 'option' );
        if ( $background_image || $background_color ) {
            $cta_style = 'background-color: ' . $background_color . '; background-image: url(\'' . $background_image . '\');';
        }

        $content = '<div class="small-cta ' . $location  . '" style="' . $cta_style . '">
            <div class="container dmbs-content">
                <div class="col-md-12 dmbs-main">' . get_field( 'small_cta_intro_content', 'option' ) . lp_get_cta_content( 'option', 'small' ) . '</div>
            </div>
        </div>';
    }
    return $content;
}

/**
 * Set excerpt length
 * @param  integer $length default excerpt length
 * @return integer modified excerpt length
 */
function lp_excerpt_length( $length ) {
    return 12;
}
add_filter( 'excerpt_length', 'lp_excerpt_length' );

/**
 * Add Bootstrap button classes to next/previous post links
 * @return string classes to add to link
 */
function lp_prev_next_post_link_class() {
    return 'class="btn btn-default"';
}
add_filter( 'previous_posts_link_attributes', 'lp_prev_next_post_link_class' );
add_filter( 'next_posts_link_attributes', 'lp_prev_next_post_link_class' );

/**
 * Get CTA button and popup modal
 * @param  mixed  $acf_type     string "option" or post ID
 * @param  string $field_prefix field prefix to use for ACF field names
 * @return string HTML CTA content
 */
function lp_get_cta_content( $acf_type, $field_prefix ) {
    $field_prefix .= '_';

    // add styles
    $cta_button_bg_color = get_field( $field_prefix . 'cta_button_background_color', $acf_type );
    $cta_button_text_color = get_field( $field_prefix . 'cta_button_text_color', $acf_type );

    $custom_cta_styles = '
    .btn.custom-cta {
        background-color: ' . $cta_button_bg_color . ';
        border-color: ' . $cta_button_bg_color . ';
        color: ' . $cta_button_text_color . ';
    }
    .modal#custom-cta .modal-content {
        background: ' . get_field( $field_prefix . 'cta_popup_background_color', $acf_type ) . ' url("' . get_field( $field_prefix . 'cta_popup_background_image', $acf_type ) . '") no-repeat center/ cover;
    }
    .modal#custom-cta .modal-content *:not(input):not(textarea) {
        color: ' . get_field( $field_prefix . 'cta_popup_text_color', $acf_type ) . ' !important;
    }
    ';

    // start content
    ob_start();
    ?>
    <style type="text/css"><?php echo $custom_cta_styles; ?></style>
    <p>
        <button class="btn btn-primary btn-lg green custom-cta" type="button" data-toggle="modal" data-target="#custom-cta"><?php the_field( $field_prefix . 'cta_button_text', $acf_type ); ?></button>
    </p>

    <div id="custom-cta" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h4 class="modal-title"><?php the_field( $field_prefix . 'cta_popup_title', $acf_type ); ?></h4>
                </div>
                <div class="modal-body">
                    <?php the_field( $field_prefix . 'cta_popup_content', $acf_type ); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
    return ob_get_clean();
}

/**
 * Add per-post CTA if specified
 * @param  string $content HTML post content
 * @return string HTML post content with CTA appended
 */
function lp_per_post_cta( $content ) {
    if ( get_field( 'show_per_post_cta' ) ) {
        $content = $content . lp_get_cta_content( get_the_ID(), 'per_post' );
    }
    return $content;
}
add_filter( 'the_content', 'lp_per_post_cta' );
