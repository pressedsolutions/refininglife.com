<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<!-- start content container -->
<div class="dmbs-content container">

    <?php

    //if this was a search we display a page header with the results count. If there were no results we display the search form.
    if (is_search()) :

        $total_results = $wp_query->found_posts;

        echo "<h2 class='page-header'>" . sprintf( __('%s Search Results for "%s"','devdmbootstrap3'),  $total_results, get_search_query() ) . "</h2>";

        if ($total_results == 0) :
            get_search_form(true);
        endif;

    endif;

    // the loop
    if ( have_posts() ) :

        // single post
        if ( is_single() ) : ?>

            <div class="col-md-12 dmbs-main">

                <?php
                while ( have_posts() ) : the_post(); ?>

                <div <?php post_class(); ?>>

                    <?php if ( ! has_post_thumbnail() ) : ?>
                        <h2 class="page-header"><?php the_title() ;?></h2>
                        <p class="post-meta"><?php echo get_the_date(); ?>  <span class="smaller">&bull;</span>  <span rel="author"><?php the_author(); ?></span></p>
                    <?php endif; ?>

                    <?php if ( get_field('youtube_url') ) : ?>
                        <div id="videop">
                            <div class="youtube_url">
                                <?php echo wp_oembed_get( get_field( 'youtube_url' ) ); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php the_content(); ?>
                    <?php wp_link_pages(); ?>
                    <?php comments_template(); ?>

                </div>
                </div><!-- .dmbs-main -->
            <?php endwhile; ?>

        <?php
        // list of posts
        else : ?>
            <div class="dmbs-main">
            <?php $counter = 1; ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php if ( $counter % 2 == 1 ) { echo '<div class="clearfix">'; } ?>
                <div <?php post_class( 'col-sm-12 col-md-6 col-lg-12' ); ?>>
                    <?php if ( has_post_thumbnail() ) : ?>
                        <div class="post-lead-img col-md-12 col-lg-6">
                            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_post_thumbnail( 'thumbnail_m' ); ?></a>
                        </div>
                    <?php endif; ?>

                    <div class="post-copy col-md-12 col-lg-6">

                        <p class="post-meta"><?php echo get_the_date(); ?>  <span class="smaller">&bull;</span>  <span rel="author"><?php the_author(); ?></span></p>
                        <h2 class="page-header">
                            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                        </h2>
                        <?php the_excerpt(); ?>

                        <p class="read-more"><a href="<?php the_permalink(); ?>" title="Read more">Continue Reading</a><span class="read-more-arrow">&nbsp;›</span></p>

                    </div><!-- .post-copy -->
                </div><!-- .post -->
                <?php if ( $counter % 2 == 0 ) { echo '</div>'; } $counter++; ?>
            <?php endwhile; ?>

                <div class="row">
                    <div class="prev-next-posts">
                        <?php posts_nav_link( ' ', '&laquo;&nbsp;Newer Posts', 'Older Posts&nbsp;&raquo;' ); ?>
                    </div>
                </div>
            </div><!-- .dmbs-main -->

        </div><!-- .dmbs-main -->

        <?php  endif; ?>

        <?php else: ?>
            <?php get_404_template(); ?>
        <?php endif; ?>

</div><!-- .dmbs-content -->
<!-- end content container -->

<?php get_footer(); ?>
