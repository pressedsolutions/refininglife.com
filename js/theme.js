(function($){
    $(document).ready(function() {
        /**
         * Remove href from a.remove-attr elements
         */
        $(".remove-attr").click(function(){
            $("a").removeAttr("href");
        });
    });
})(jQuery);
