<?php
/*
Template Name: Home Page
*/

get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php
// large CTA
if ( get_field( 'large_cta_intro_content', 'option' ) ) {
    $background_image = get_field( 'large_cta_background_image', 'option' );
    $background_color = get_field( 'large_cta_background_color', 'option' );
    if ( $background_image || $background_color ) {
        $cta_style = 'background-color: ' . $background_color . '; background-image: url(\'' . $background_image . '\');';
    }

    echo '<div class="large-cta" style="' . $cta_style . '">
        <div class="container dmbs-content">
            <div class="row">
                <div class="col-xs-12 col-md-8">' . get_field( 'large_cta_intro_content', 'option' ) . lp_get_cta_content( 'option', 'large' ) . '</div>
            </div>
        </div>
    </div>';
}
?>

<!-- start content container -->
<div class="container dmbs-content">

    <div class="col-md-12 dmbs-main">

        <?php
        // the loop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="page-header"><?php the_title() ;?></h2>
            <?php the_content(); ?>
            <?php wp_link_pages(); ?>
            <?php comments_template(); ?>

        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>
