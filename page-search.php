<?php

/* Template Name: Search (full-width) */

add_filter( 'body_class', 'rl_search_class' );
function rl_search_class( $classes ) {
    $classes[] = 'search full-width';
    return $classes;
}

get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>



<!-- start content container -->
<div class="container dmbs-content">

    <div class="col-md-12 dmbs-main">

        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="page-header"><?php the_title() ;?></h2>
            <?php the_content(); ?>
            <div class="row">
                <div class="col-md-12 dmbs-left search-widgets">
                    <?php dynamic_sidebar( 'searchfull' ); ?>
                </div>
            </div>

        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>
