<?php

/* Template Name: With Meta Full Width */

get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>



<!-- start content container -->
<div class="container nopadding dmbs-content">

    <div class="col-md-12 dmbs-main">

        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="page-header"><?php the_title() ;?></h2>
            <?php get_template_part('template-part', 'postmeta'); ?>
                <div class="post">
                    <?php the_content(); ?>
                    <?php wp_link_pages(); ?>
                    <?php comments_template(); ?>
                </div>
                <div class="bottommeta">
                    <?php get_template_part('template-part', 'postmeta'); ?>
                </div>
        <?php endwhile; ?>
        <?php else: ?>

    <?php get_404_template(); ?>

    <?php endif; ?>

    </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>
