<?php global $dm_settings; ?>

<div class="dmbs-container">

<?php if ($dm_settings['show_header'] != 0) : ?>

    <div class="dmbs-header">
        <div class="container">
            <div class="row homecontainer">

                <?php if ( get_header_image() != '' || get_header_textcolor() != 'blank') : ?>

                    <div class="col-sm-4 headerlogo dmbs-header-img">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo ( get_header_image() ? get_header_image_tag() : bloginfo( 'name' ) ); ?></a>
                    </div>
                <?php endif; ?>

                <div class="col-sm-8 dmbs-header-text">
                    <?php get_template_part( 'template-part', 'topnav' ); ?>
                </div>

            </div>
        </div>
    </div>
<?php endif; ?>

<?php
if ( ! is_page_template( 'page-home.php' ) && ! is_single() && get_field( 'show_cta' ) ) {
    echo get_small_cta( 'top' );
} elseif ( is_single() && has_post_thumbnail() ) {
    echo '<div class="full-width-featured-image" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(' . get_the_post_thumbnail_url( get_the_ID(), 'thumbnail_xl' ) . ')">
        <h2 class="page-header">' . get_the_title() . '</h2>
        <p class="post-meta">' . get_the_date() . '  <span class="smaller">&bull;</span>  <span class="author" rel="author">' . get_the_author() . '</span></p>
    </div>';
}
