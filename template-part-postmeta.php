<?php global $dm_settings; ?>
<?php if ($dm_settings['show_postmeta'] != 0) : ?>
<div class="footer-col row">
    <div class="col-md-12 author-meta">
        <?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
        <p>Written by<br>
           <?php the_author_posts_link(); ?>
        </p>
    </div>
</div>
<?php endif; ?>
