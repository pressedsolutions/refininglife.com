if [ ! -f "database-latest.sql.gz" ]; then
    ssh RefiningLife 'DATE=$(date +"%Y-%m-%d-%H%M") && mysqldump --defaults-file=/root/.mysql.conf -u wordpress wordpress | gzip > /root/database-backups/database-$DATE.sql.gz && ln -f -s /root/database-backups/database-$DATE.sql.gz /root/database-latest.sql.gz' && scp -C RefiningLife:/root/database-latest.sql.gz ./
fi

# import
gzcat database-latest.sql.gz | mysql --defaults-file=~/.mysql_credentials refininglife && mv database-latest.sql.gz ~/.Trash/

# disable plugins
cd ~/Sites/refininglife/
wp search-replace "https://dev.refininglife.com" "http://refininglife.dev"
wp plugin deactivate login-security-solution wp-super-cache updraftplus
